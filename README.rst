ImaticViewBundle
================

See *Resources/doc/index.rst* for documentation.

## Build Process

In case any asset sources are changed run ``yarn build`` command which will compile them.
Any compiled assets are commited with project. Build process is provided by standard webpack configuration.
More details can be found in ``webpack.config.js``.
