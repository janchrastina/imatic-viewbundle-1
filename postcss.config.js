module.exports = {
    plugins: [
        /*
         * Supported browsers are determined by browserslist query in package.json.
         */
        require('autoprefixer')()
    ]
};
